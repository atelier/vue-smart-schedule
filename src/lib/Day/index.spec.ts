import Day from './index'
import Weekday from '@/lib/Weekday'
import Time from '@/lib/Time'
import TimeRange from '@/lib/TimeRange'

const seven = new Time('7')
const nine = new Time('9')
const eleven = new Time('11')
const noon = new Time('12')
const three = new Time('15')
const five = new Time('17')

describe('Correct times', () => {
  it('creates correct time objects', () => {
    expect(seven.time).toEqual(420)
    expect(nine.time).toEqual(540)
    expect(eleven.time).toEqual(660)
    expect(noon.time).toEqual(720)
    expect(three.time).toEqual(900)
    expect(five.time).toEqual(1020)
  })
})

describe('Modifying business hours', () => {
  it('throws on overlapping business and break hours start', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.breakHours = new TimeRange(eleven, three)
      day.hasBreakHours = true
      day.businessHours = new TimeRange(noon, five)
    }).toThrow()
  })

  it('throws on overlapping business and break hours end', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.breakHours = new TimeRange(noon, five)
      day.hasBreakHours = true
      day.businessHours = new TimeRange(nine, three)
    }).toThrow()
  })

  it('throws on overlapping business and break hours start and end', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.breakHours = new TimeRange(nine, five)
      day.hasBreakHours = true
      day.businessHours = new TimeRange(eleven, three)
    }).toThrow()
  })
})

describe('Modifying break hours', () => {
  it('sets overlapping break hours when hasBusinessHours is false', () => {
    const day = new Day(Weekday.MONDAY)
    day.businessHours = new TimeRange(nine, three)
    day.hasBusinessHours = false
    day.breakHours = new TimeRange(seven, five)
    expect(day.hasBusinessHours).toBe(false)
    expect(day.hasBreakHours).toBe(false)
    expect(day.businessHours.start.isSame(nine)).toBe(true)
    expect(day.businessHours.end.isSame(three)).toBe(true)
    expect(day.breakHours.start.isSame(seven)).toBe(true)
    expect(day.breakHours.end.isSame(five)).toBe(true)
  })

  it('throws on overlapping business and break hours start', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.businessHours = new TimeRange(noon, five)
      day.breakHours = new TimeRange(eleven, three)
    }).toThrow()
  })

  it('throws on overlapping business and break hours end', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.businessHours = new TimeRange(nine, three)
      day.breakHours = new TimeRange(noon, five)
    }).toThrow()
  })

  it('throws on overlapping business and break hours start and end', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.businessHours = new TimeRange(eleven, three)
      day.breakHours = new TimeRange(nine, five)
    }).toThrow()
  })
})

describe('Modifying hasBreakHours', () => {
  it('correctly sets hasBreakHours if all ranges are valid', () => {
    const day = new Day(Weekday.MONDAY)
    day.businessHours = new TimeRange(eleven, five)
    day.hasBusinessHours = false
    day.breakHours = new TimeRange(noon, three)
    day.hasBusinessHours = true
    day.hasBreakHours = true
    expect(day.businessHours.start.isSame(eleven)).toBe(true)
    expect(day.businessHours.end.isSame(five)).toBe(true)
    expect(day.breakHours.start.isSame(noon)).toBe(true)
    expect(day.breakHours.end.isSame(three)).toBe(true)
    expect(day.hasBusinessHours).toBe(true)
    expect(day.hasBreakHours).toBe(true)
  })

  it('throws on setting to true when hasBusinessHours is false', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.businessHours = new TimeRange(noon, five)
      day.breakHours = new TimeRange(eleven, three)
    }).toThrow()
  })

  it('throws on setting to true when businessHours doesn\'t include previously set breakHours', () => {
    expect(() => {
      const day = new Day(Weekday.MONDAY)
      day.businessHours = new TimeRange(noon, five)
      day.hasBusinessHours = false
      day.breakHours = new TimeRange(eleven, three)
      day.hasBusinessHours = true
      day.hasBreakHours = true
    }).toThrow()
  })
})
