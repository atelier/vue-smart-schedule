import Time from '@/lib/Time'
import TimeRange from '@/lib/TimeRange'
import Weekday from '@/lib/Weekday'
import { WEEKEND } from '@/config'

export interface IDay {
  weekday: Weekday
  businessHours: TimeRange,
  breakHours: TimeRange,
  hasBusinessHours: boolean
  hasBreakHours: boolean
}

export default class Day implements IDay {
  readonly ownWeekday: Weekday
  private ownBusinessHours: TimeRange
  private ownBreakHours: TimeRange
  private ownHasBusinessHours: boolean
  private ownHasBreakHours: boolean

  constructor(weekday: Weekday = Weekday.ANY) {
    this.ownWeekday = weekday
    this.ownBusinessHours = new TimeRange(new Time(), new Time())
    this.ownBreakHours = new TimeRange(new Time(), new Time())
    this.ownHasBusinessHours = !WEEKEND.includes(weekday)
    this.ownHasBreakHours = false
  }

  public get weekday() {
    return this.ownWeekday
  }

  public get isWeekend() {
    return WEEKEND.includes(this.ownWeekday)
  }

  public get businessHours() {
    return this.ownBusinessHours
  }

  public set businessHours(to: TimeRange) {
    if (this.ownHasBreakHours && !to.includes(this.ownBreakHours)) {
      throw new Error('Overlapping business and break hours')
    } else {
      this.ownBusinessHours = to
    }
  }

  public get breakHours() {
    return this.ownBreakHours
  }

  public set breakHours(to: TimeRange) {
    if (this.ownHasBusinessHours && this.ownHasBreakHours) {
      if (this.ownBusinessHours.includes(to)) {
        this.ownBreakHours = to
      } else {
        throw new Error('Business hours does not include provided break hours')
      }
    } else {
      this.ownBreakHours = to
    }
  }

  public get hasBusinessHours() {
    return this.ownHasBusinessHours
  }

  public set hasBusinessHours(to: boolean) {
    if (!to) this.ownHasBreakHours = to
    this.ownHasBusinessHours = to
  }

  public get hasBreakHours() {
    return this.ownHasBreakHours
  }

  public set hasBreakHours(to: boolean) {
    if (to) {
      if (this.ownHasBusinessHours) {
        if (this.ownBusinessHours.includes(this.ownBreakHours)) {
          this.ownHasBreakHours = to
        } else {
          throw new Error('Can\'t set hasBreakHours to true when ownBusinessHours doesn\'t include ownBreakHours')
        }
      } else {
        throw new Error('Can\'t set hasBreakHours to true when hasBusinessHours is false')
      }
    } else {
      this.ownHasBreakHours = to
    }
  }
}
