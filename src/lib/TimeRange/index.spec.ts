import TimeRange from '@/lib/TimeRange'
import Time from '@/lib/Time'
import { TIME_RANGE_SEPARATOR, TIME_SEPARATOR } from '@/config'

describe('Constructor', () => {
  it('creates range without arguments', () => {
    const range = new TimeRange()
    expect(range.start.time).toEqual(0)
    expect(range.end.time).toEqual(0)
  })

  it('creates range with both arguments', () => {
    const range = new TimeRange(new Time('7'), new Time('11'))
    expect(range.start.time).toEqual(420)
    expect(range.end.time).toEqual(660)
  })

  it('throws on missing argument', () => {
    expect(() => {
      const range = new TimeRange(new Time('11'))
    }).toThrow()
  })
})

describe('Generic time range', () => {
  const seven = new Time('7')
  const eleven = new Time('11')
  const timeRange = new TimeRange(seven, eleven)

  it('outputs correct timeString', () => {
    expect(timeRange.asTimeRangeString).toBe(`07${TIME_SEPARATOR}00${TIME_RANGE_SEPARATOR}11${TIME_SEPARATOR}00`)
  })

  it('outputs correct length', () => {
    expect(timeRange.length).toEqual(240)
  })

  it('outputs correct length timestring', () => {
    expect(timeRange.lengthAsTimeString).toBe(`04${TIME_SEPARATOR}00`)
  })

  it('outputs correct length timeobject', () => {
    const length = timeRange.lengthAsTimeObject
    expect(length.asTimeString).toBe(`04${TIME_SEPARATOR}00`)
    expect(length.value).toEqual(240)
  })
})

describe('Overnight time range', () => {
  const noon = new Time('12')
  const halfPastMidnight = new Time('00:30')
  const timeRange = new TimeRange(noon, halfPastMidnight)

  it('outputs correct timeString', () => {
    expect(timeRange.asTimeRangeString).toBe(`12${TIME_SEPARATOR}00${TIME_RANGE_SEPARATOR}00${TIME_SEPARATOR}30`)
  })

  it('outputs correct length', () => {
    expect(timeRange.length).toEqual(750)
  })

  it('outputs correct length timestring', () => {
    expect(timeRange.lengthAsTimeString).toBe(`12${TIME_SEPARATOR}30`)
  })

  it('outputs correct length timeobject', () => {
    const length = timeRange.lengthAsTimeObject
    expect(length.asTimeString).toBe(`12${TIME_SEPARATOR}30`)
    expect(length.value).toEqual(750)
  })
})

describe('Full day time range', () => {
  const midnight = new Time('0')
  const timeRange = new TimeRange(midnight, midnight)

  it('outputs correct timeString', () => {
    expect(timeRange.asTimeRangeString).toBe(`00${TIME_SEPARATOR}00${TIME_RANGE_SEPARATOR}00${TIME_SEPARATOR}00`)
  })

  it('outputs correct length', () => {
    expect(timeRange.length).toEqual(1440)
  })

  it('outputs correct length timestring', () => {
    expect(timeRange.lengthAsTimeString).toBe(`24${TIME_SEPARATOR}00`)
  })

  it('outputs correct length timeobject', () => {
    const length = timeRange.lengthAsTimeObject
    expect(length.asTimeString).toBe(`24${TIME_SEPARATOR}00`)
    expect(length.value).toEqual(1440)
  })
})

describe('Detects when TimeRange', () => {
  it('is full day', () => {
    expect(new TimeRange(new Time(), new Time()).isFullDay).toBe(true)
  })
})

describe('TimeRange comparison', () => {
  const seven = new Time('7')
  const nine = new Time('9')
  const eleven = new Time('11')
  const three = new Time('15')
  const five = new Time('17')

  it('compares same ranges correctly', () => {
    const rangeOne = new TimeRange(seven, eleven)
    const rangeTwo = new TimeRange(seven, eleven)
    expect(rangeOne.isSame(rangeTwo)).toBe(true)
  })

  it('compares different ranges correctly', () => {
    const rangeOne = new TimeRange(seven, eleven)
    const rangeTwo = new TimeRange(seven, three)
    expect(rangeOne.isSame(rangeTwo)).toBe(false)
  })

  it('compares different ranges with same length correctly', () => {
    const rangeOne = new TimeRange(seven, eleven)
    const rangeTwo = new TimeRange(eleven, three)
    expect(rangeOne.isSame(rangeTwo)).toBe(false)
    expect(rangeOne.isSameLength(rangeTwo)).toBe(true)
  })

  it('compares ranges inclusiveness', () => {
    const sevenToThree = new TimeRange(seven, three)
    const nineToEleven = new TimeRange(nine, eleven)
    expect(sevenToThree.includes(nineToEleven)).toBe(true)
    expect(nineToEleven.includes(sevenToThree)).toBe(false)
  })

  it('compares time inclusiveness', () => {
    const sevenToThree = new TimeRange(seven, three)
    expect(sevenToThree.includes(eleven)).toBe(true)
    expect(sevenToThree.includes(five)).toBe(false)
  })

  it('compares full day time and range inclusiveness', () => {
    const fullDay = new TimeRange(new Time(), new Time())
    const sevenToThree = new TimeRange(seven, three)
    expect(fullDay.includes(sevenToThree)).toBe(true)
    expect(fullDay.includes(five)).toBe(true)
  })

  it('compares ranges intersection', () => {
    const sevenToThree = new TimeRange(seven, three)
    const sevenToEleven = new TimeRange(seven, eleven)
    const nineToEleven = new TimeRange(nine, eleven)
    const nineToFive = new TimeRange(nine, five)
    const elevenToFive = new TimeRange(eleven, five)
    const threeToFive = new TimeRange(three, five)

    // includes both
    expect(sevenToThree.intersects(nineToEleven)).toBe(true)
    // includes none
    expect(threeToFive.intersects(nineToEleven)).toBe(false)
    // includes start
    expect(sevenToThree.intersects(nineToFive)).toBe(true)
    // includes end
    expect(elevenToFive.intersects(sevenToThree)).toBe(true)
    // common boundaries
    expect(sevenToEleven.intersects(elevenToFive)).toBe(false)
    // includes start and has common end
    expect(sevenToEleven.intersects(nineToEleven)).toBe(true)
    // includes end and has common start
    expect(nineToFive.intersects(nineToEleven)).toBe(true)
  })
})
