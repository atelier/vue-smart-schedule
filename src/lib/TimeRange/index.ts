import Time, { TTimeString, TTimeValue } from '@/lib/Time'
import { TIME_RANGE_SEPARATOR } from '@/config'

export interface ITimeRange {
  asTimeRangeString: TTimeString
  asCompactTimeRangeString: string
  length: TTimeValue
  lengthAsTimeObject: Time
  lengthAsTimeString: TTimeString
  isSame(as: TimeRange): boolean
  isSameLength(as: TimeRange): boolean
  isFullDay: boolean
  includes(timeOrRange: Time | TimeRange): boolean
  intersects(range: TimeRange): boolean
}

export default class TimeRange implements ITimeRange {
  readonly start: Time
  readonly end: Time

  constructor(start?: Time, end?: Time) {
    if (!start && !end) {
      this.start = new Time()
      this.end = new Time()
    } else if (!!start && !!end) {
      this.start = start
      this.end = end
    } else {
      throw new Error('Wrong arguments number')
    }
  }

  public get asTimeRangeString() {
    return this.start.asTimeString + TIME_RANGE_SEPARATOR + this.end.asTimeString
  }

  public get asCompactTimeRangeString() {
    return this.start.asCompactTimeString + TIME_RANGE_SEPARATOR + this.end.asCompactTimeString
  }

  public get length() {
    let diff = this.end.value - this.start.value
    if (!this.end.isAfter(this.start)) diff += 1440
    return diff
  }

  public get lengthAsTimeObject() {
    return Time.fromValue(this.length)
  }

  public get lengthAsTimeString() {
    return this.lengthAsTimeObject.asTimeString
  }

  public isSame(as: TimeRange) {
    return this.start.value === as.start.value && this.end.value === as.end.value
  }

  public includes(timeOrRange: Time | TimeRange) {
    if (timeOrRange instanceof Time) {
      if (this.isFullDay) {
        return timeOrRange.isAfter(this.start)
      }
      return timeOrRange.isAfter(this.start) && timeOrRange.isBefore(this.end)
    }
    if (this.isFullDay) {
      return timeOrRange.start.isAfter(this.start) && timeOrRange.end.isAfter(this.start)
    }
    return timeOrRange.start.isAfter(this.start) && timeOrRange.end.isBefore(this.end)
  }

  public intersects(range: TimeRange) {
    return this.includes(range.start) || this.includes(range.end)
  }

  public isSameLength(as: TimeRange) {
    return this.length === as.length
  }

  public get isFullDay() {
    return this.length === 1440
  }
}
