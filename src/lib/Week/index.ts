import { WEEKDAYS_ORDER } from '@/config'
import Day from '@/lib/Day'
import Weekday from '@/lib/Weekday'
import TimeRange from '@/lib/TimeRange'
import Time from '@/lib/Time'

export interface IWeek {
  baseBusinessHours: TimeRange
  baseBreakHours: TimeRange
  hasBreakHours: boolean
}

export default class Week implements IWeek {
  readonly ownDays: Record<Weekday, Day>
  private ownBaseDay: Day

  constructor() {
    this.ownBaseDay = new Day()
    this.ownDays = WEEKDAYS_ORDER.reduce(
      (acc: Record<Weekday, Day>, val: Weekday): Record<Weekday, Day> => {
        acc[val] = new Day(val)
        return acc
      },
      {} as Record<Weekday, Day>,
    )
  }

  public get days() {
    return this.ownDays
  }

  public get baseBusinessHours() {
    return this.ownBaseDay.businessHours
  }

  public set baseBusinessHours(range: TimeRange) {
    WEEKDAYS_ORDER.map((weekdayName) => {
      if (this.ownDays[weekdayName].businessHours.isSame(this.ownBaseDay.businessHours)) {
        this.ownDays[weekdayName].businessHours = range
      }
      return true
    })
    this.ownBaseDay.businessHours = range
  }

  public get baseBreakHours() {
    return this.ownBaseDay.breakHours
  }

  public set baseBreakHours(range: TimeRange) {
    WEEKDAYS_ORDER.map((weekdayName) => {
      if (this.ownDays[weekdayName].breakHours.isSame(this.ownBaseDay.breakHours)) {
        this.ownDays[weekdayName].breakHours = range
        this.ownDays[weekdayName].hasBreakHours = this.ownDays[weekdayName].hasBusinessHours
      }
      return true
    })
    this.ownBaseDay.breakHours = range
    this.ownBaseDay.hasBreakHours = true
  }

  public get hasBreakHours() {
    return this.ownBaseDay.hasBreakHours
  }

  public set hasBreakHours(to: boolean) {
    if (to) {
      WEEKDAYS_ORDER.map((weekdayName) => {
        if (this.ownDays[weekdayName].hasBusinessHours) {
          this.ownDays[weekdayName].breakHours = this.ownBaseDay.breakHours
          this.ownDays[weekdayName].hasBreakHours = to
        }
        return true
      })
    } else {
      WEEKDAYS_ORDER.map((weekdayName) => {
        this.ownDays[weekdayName].hasBreakHours = to
        return true
      })
    }
    this.ownBaseDay.hasBreakHours = to
  }
}
