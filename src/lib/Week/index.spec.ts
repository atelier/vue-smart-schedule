import Week from './index'
import Weekday from '@/lib/Weekday'
import { WEEKEND } from '@/config'
import TimeRange from '@/lib/TimeRange'
import Time from '@/lib/Time'


describe('Creates correct week', () => {
  it('creates correct days in week', () => {
    const week = new Week()
    Object.keys(week.days).map((weekdayName) => {
      const {
        businessHours,
        breakHours,
        hasBusinessHours,
        hasBreakHours,
      } = week.days[weekdayName as Weekday]
      const isWeekend: boolean = WEEKEND.includes(weekdayName as Weekday)

      expect(hasBusinessHours).toBe(!isWeekend)
      expect(hasBreakHours).toBe(false)
      expect(businessHours.length).toEqual(1440)
      expect(breakHours.length).toEqual(1440)
      return false
    })
  })
})

describe('Business hours', () => {
  it('sets initial base business hours correctly', () => {
    const week = new Week()
    const nine = new Time('9')
    const five = new Time('17')

    week.baseBusinessHours = new TimeRange(nine, five)

    expect(week.days[Weekday.MONDAY].businessHours.start.isSame(nine)).toBe(true)
    expect(week.days[Weekday.MONDAY].businessHours.end.isSame(five)).toBe(true)

    expect(week.days[Weekday.SUNDAY].businessHours.start.isSame(nine)).toBe(true)
    expect(week.days[Weekday.SUNDAY].businessHours.end.isSame(five)).toBe(true)

    expect(week.days[Weekday.TUESDAY].hasBusinessHours).toBe(true)
    expect(week.days[Weekday.SUNDAY].hasBusinessHours).toBe(false)
  })

  it('sets already changed base business hours correctly', () => {
    const week = new Week()
    const nine = new Time('9')
    const five = new Time('17')
    const six = new Time('18')

    week.baseBusinessHours = new TimeRange(nine, five)
    week.baseBusinessHours = new TimeRange(nine, six)

    expect(week.days[Weekday.MONDAY].businessHours.start.isSame(nine)).toBe(true)
    expect(week.days[Weekday.MONDAY].businessHours.end.isSame(six)).toBe(true)

    expect(week.days[Weekday.SUNDAY].businessHours.start.isSame(nine)).toBe(true)
    expect(week.days[Weekday.SUNDAY].businessHours.end.isSame(six)).toBe(true)

    expect(week.days[Weekday.MONDAY].hasBusinessHours).toBe(true)
    expect(week.days[Weekday.SUNDAY].hasBusinessHours).toBe(false)
  })
})

describe('Break hours', () => {
  const two = new Time('14')
  const three = new Time('15')

  it('sets initial base break hours correctly', () => {
    const week = new Week()

    week.baseBreakHours = new TimeRange(two, three)

    expect(week.days[Weekday.MONDAY].breakHours.start.isSame(two)).toBe(true)
    expect(week.days[Weekday.MONDAY].breakHours.end.isSame(three)).toBe(true)

    expect(week.days[Weekday.SUNDAY].breakHours.start.isSame(two)).toBe(true)
    expect(week.days[Weekday.SUNDAY].breakHours.end.isSame(three)).toBe(true)

    expect(week.days[Weekday.MONDAY].hasBreakHours).toBe(true)
    expect(week.days[Weekday.SUNDAY].hasBreakHours).toBe(false)
  })

  it('sets base break hours correctly', () => {
    const week = new Week()
    const nine = new Time('9')
    const five = new Time('17')

    week.baseBusinessHours = new TimeRange(nine, five)
    week.days[Weekday.WEDNESDAY].hasBusinessHours = false

    week.baseBreakHours = new TimeRange(two, three)

    expect(week.days[Weekday.WEDNESDAY].hasBreakHours).toBe(false)

    week.hasBreakHours = false

    expect(week.days[Weekday.MONDAY].hasBreakHours).toBe(false)
  })
})
