import Time, { tupleToTime, timeToTuple, parseTimeString, validateHours, validateMinutes, zeroFill } from '@/lib/Time'
import { TIME_SEPARATOR } from '@/config'

describe('String parsing', () => {
  it('throws on one NaN', () => {
    expect(() => {
      parseTimeString('something')
    }).toThrow()
  })

  it('throws on several NaNs', () => {
    expect(() => {
      parseTimeString(`something${TIME_SEPARATOR}more`)
    }).toThrow()
  })

  it('throws on invalid hours', () => {
    expect(() => {
      parseTimeString(`24${TIME_SEPARATOR}05`)
    }).toThrow()
  })

  it('throws on invalid minutes', () => {
    expect(() => {
      parseTimeString(`13${TIME_SEPARATOR}-15`)
    }).toThrow()
  })

  it('parses 0', () => {
    expect(parseTimeString('0')).toStrictEqual(0)
  })

  it('parses 0:0', () => {
    expect(parseTimeString(`0${TIME_SEPARATOR}0`)).toStrictEqual(0)
  })

  it('parses 00:00', () => {
    expect(parseTimeString(`00${TIME_SEPARATOR}00`)).toStrictEqual(0)
  })

  it('parses just hour', () => {
    expect(parseTimeString('12')).toBe(720)
  })

  it('parses hour:minute', () => {
    expect(parseTimeString(`21${TIME_SEPARATOR}17`)).toBe(1277)
  })

  it('parses hour:minute:second', () => {
    expect(parseTimeString(`00${TIME_SEPARATOR}15${TIME_SEPARATOR}30`)).toBe(15)
  })

  it('parses hour:minuteBelowTen', () => {
    expect(parseTimeString(`5${TIME_SEPARATOR}5`)).toBe(305)
  })
})

describe('Hours validation', () => {
  it('validates zero hours as true', () => {
    expect(validateHours(0)).toBe(true)
  })

  it('validates some midday hours as true', () => {
    expect(validateHours(7)).toBe(true)
  })

  it('validates below zero hours as false', () => {
    expect(validateHours(-1)).toBe(false)
  })

  it('validates 24 hours as true', () => {
    expect(validateHours(24)).toBe(true)
  })

  it('validates more than 24 hours', () => {
    expect(validateHours(25)).toBe(false)
  })
})

describe('Minutes validation', () => {
  it('validates zero minutes as true', () => {
    expect(validateMinutes(0)).toBe(true)
  })

  it('validates some mid-hour minutes as true', () => {
    expect(validateMinutes(25)).toBe(true)
  })

  it('validates below zero minutes as false', () => {
    expect(validateMinutes(-1)).toBe(false)
  })

  it('validates 60 minutes as false', () => {
    expect(validateMinutes(60)).toBe(false)
  })

  it('validates more than 60 minutes', () => {
    expect(validateMinutes(61)).toBe(false)
  })
})

describe('Tuple to time conversion', () => {
  it('throws on more than 24 hours', () => {
    expect(() => {
      tupleToTime([24, 1])
    }).toThrow()
  })

  it('converts generic tuple to time', () => {
    expect(tupleToTime([5, 40])).toBe(340)
  })

  it('converts zero-hour tuple to time', () => {
    expect(tupleToTime([0, 45])).toBe(45)
  })

  it('converts zero-minute tuple to time', () => {
    expect(tupleToTime([2, 0])).toBe(120)
  })
})

describe('Time to tuple conversion', () => {
  it('throws on more than 24 hours', () => {
    expect(() => {
      timeToTuple(1441)
    }).toThrow()
  })

  it('converts time to tuple', () => {
    expect(timeToTuple(280)).toEqual([4, 40])
  })

  it('converts zero time to tuple', () => {
    expect(timeToTuple(0)).toEqual([0, 0])
  })

  it('converts 24 hours to tuple', () => {
    expect(timeToTuple(1440)).toEqual([24, 0])
  })
})


describe('Filling with zeros', () => {
  it('fills up to 00 by default', () => {
    expect(zeroFill()).toBe('00')
  })

  it('fills up to 2 digits by default if zero', () => {
    expect(zeroFill(0)).toBe('00')
  })

  it('fills up to 2 digits by default below ten', () => {
    expect(zeroFill(2)).toBe('02')
  })

  it('doesn\'t fill by default above ten', () => {
    expect(zeroFill(12)).toBe('12')
  })

  it('doesn\'t fill by default above hundred', () => {
    expect(zeroFill(125)).toBe('125')
  })

  it('fills up to provided target length if zero', () => {
    expect(zeroFill(0, 3)).toBe('000')
  })

  it('doesn\'t fill if provided length is same as number', () => {
    expect(zeroFill(235, 3)).toBe('235')
  })

  it('doesn\'t fill if provided length is less than number', () => {
    expect(zeroFill(560, 2)).toBe('560')
  })
})

describe('Object empty initializer', () => {
  it('outputs timeString on empty initializer', () => {
    const time = new Time()
    expect(time.asTimeString).toBe(`00${TIME_SEPARATOR}00`)
  })
})

describe('Object fromValue initializer', () => {
  it('initializes time object from number', () => {
    const time = Time.fromValue(875)
    expect(time.value).toBe(875)
    expect(time.asTimeString).toBe(`14${TIME_SEPARATOR}35`)
  })
})

describe('Object value output', () => {
  it('outputs value on timestring', () => {
    const time = new Time('12:25')
    expect(time.value).toBe(745)
  })
})

describe('Object timestring output', () => {
  it('outputs timeString on timeString', () => {
    const time = new Time(`23${TIME_SEPARATOR}35`)
    expect(time.asTimeString).toBe(`23${TIME_SEPARATOR}35`)
  })

  it('outputs timestring on just hours', () => {
    const time = new Time('12')
    expect(time.asTimeString).toBe(`12${TIME_SEPARATOR}00`)
  })

  it('outputs timestring on zero hours', () => {
    const time = new Time('0')
    expect(time.asTimeString).toBe(`00${TIME_SEPARATOR}00`)
  })

  it('outputs timestring on below ten minutes', () => {
    const time = new Time(`12${TIME_SEPARATOR}5`)
    expect(time.asTimeString).toBe(`12${TIME_SEPARATOR}05`)
  })
})

describe('Object compact timestring output', () => {
  it('outputs compact timestring on timestring', () => {
    const time = new Time(`23${TIME_SEPARATOR}35`)
    expect(time.asCompactTimeString).toBe(`23${TIME_SEPARATOR}35`)
  })

  it('outputs just hours on just hours', () => {
    const time = new Time('12')
    expect(time.asCompactTimeString).toBe('12')
  })

  it('outputs zero on zero hours', () => {
    const time = new Time('0')
    expect(time.asCompactTimeString).toBe('0')
  })

  it('outputs compact timestring on below ten minutes', () => {
    const time = new Time(`12${TIME_SEPARATOR}5`)
    expect(time.asCompactTimeString).toBe(`12${TIME_SEPARATOR}05`)
  })
})

describe('Object hours output', () => {
  it('outputs hours on below ten hours', () => {
    const time = new Time('2')
    expect(time.hours).toBe('02')
  })

  it('outputs hours on above ten hours', () => {
    const time = new Time('14')
    expect(time.hours).toBe('14')
  })

  it('outputs hours on zero', () => {
    const time = new Time('0')
    expect(time.hours).toBe('00')
  })

  it('outputs hours on below ten minutes', () => {
    const time = new Time(`15${TIME_SEPARATOR}5`)
    expect(time.hours).toBe('15')
  })
})

describe('Object minutes output', () => {
  it('outputs minutes on just hours', () => {
    const time = new Time('2')
    expect(time.minutes).toBe('00')
  })

  it('outputs minutes on above ten minutes', () => {
    const time = new Time(`14${TIME_SEPARATOR}45`)
    expect(time.minutes).toBe('45')
  })

  it('outputs zeros as minutes on zero', () => {
    const time = new Time('0')
    expect(time.minutes).toBe('00')
  })

  it('outputs zero-filled minutes on below ten minutes', () => {
    const time = new Time(`15${TIME_SEPARATOR}5`)
    expect(time.minutes).toBe('05')
  })
})

describe('Object comparisons', () => {
  it('compares correctly with isBefore', () => {
    const seven = new Time('7')
    const eleven = new Time('11')
    expect(seven.isBefore(eleven)).toBe(true)
    expect(eleven.isBefore(seven)).toBe(false)
  })

  it('compares correctly with isAfter', () => {
    const threeFifteen = new Time('15:15')
    const noon = new Time('12')
    expect(threeFifteen.isAfter(noon)).toBe(true)
    expect(noon.isAfter(threeFifteen)).toBe(false)
  })

  it('compares correctly with isSame', () => {
    const twelveThirty = new Time('12:30')
    const halfPastNoon = new Time('12:30')
    const seven = new Time('7')
    expect(twelveThirty.isSame(halfPastNoon)).toBe(true)
    expect(seven.isSame(twelveThirty)).toBe(false)
  })
})
