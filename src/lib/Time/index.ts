import { TIME_SEPARATOR } from '@/config'

export type TTimeValue = number
export type TTimeString = string
export type TTimeTuple = [number, number]


export function validateHours(hours: number): boolean {
  return hours > -1 && hours < 25
}

export function validateMinutes(minutes: number): boolean {
  return minutes > -1 && minutes < 60
}

export function tupleToTime(tuple: TTimeTuple): TTimeValue | never {
  const time = tuple[0] * 60 + tuple[1]
  if (time > 1440) throw new Error('Can\'t be more than 24:00')
  return time
}

export function timeToTuple(time: TTimeValue): TTimeTuple {
  if (time > 1440) throw new Error('Can\'t be more than 24:00')
  const minutes = time % 60
  const hours = (time - minutes) / 60
  return [hours, minutes]
}

export function parseTimeString(timeStringLike: string): TTimeValue | never {
  let candidate = timeStringLike
    .split(TIME_SEPARATOR)
    .map(s => parseInt(s, 10))

  if (candidate.length === 1) {
    candidate = [candidate[0], 0]
  }

  if (candidate.some(n => Number.isNaN(n))) {
    throw new Error('Invalid number in string')
  }

  if (validateHours(candidate[0]) && validateMinutes(candidate[1])) {
    return tupleToTime([candidate[0], candidate[1]])
  }
  throw new Error('Invalid minutes or hours')
}

export function zeroFill(number: number = 0, targetLength: number = 2): string {
  const absoluteNumberAsString: string = `${Math.abs(number)}`
  const zerosToFill: number = targetLength - absoluteNumberAsString.length
  return (10 ** Math.max(0, zerosToFill)).toString().substr(1) + absoluteNumberAsString
}

export interface ITime {
  value: TTimeValue
  asTimeString: TTimeString
  asCompactTimeString: string
  hours: string
  minutes: string
  isBefore (than: Time): boolean
  isAfter (than: Time): boolean
  isSame (as: Time): boolean
}

export default class Time implements ITime {
  readonly time: TTimeValue

  constructor(timeStringLike: string = '0') {
    this.time = parseTimeString(timeStringLike)
  }

  public static fromValue(value: TTimeValue): Time {
    return new this(timeToTuple(value).map(n => zeroFill(n)).join(TIME_SEPARATOR))
  }

  public get value() {
    return this.time
  }

  public get asTimeString() {
    return timeToTuple(this.time).map(n => zeroFill(n)).join(TIME_SEPARATOR)
  }

  public get asCompactTimeString() {
    const tuple = timeToTuple(this.time)
    if (tuple[1] === 0) {
      return tuple[0].toString()
    }
    return tuple[0].toString() + TIME_SEPARATOR + zeroFill(tuple[1])
  }

  public get hours() {
    return zeroFill(timeToTuple(this.time)[0])
  }

  public get minutes() {
    return zeroFill(timeToTuple(this.time)[1])
  }

  public isBefore(than: Time) {
    return this.value < than.value
  }

  public isAfter(than: Time) {
    return this.value > than.value
  }

  public isSame(as: Time) {
    return this.value === as.value
  }
}
