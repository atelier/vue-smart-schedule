import Weekday from '@/lib/Weekday'

export const TIME_SEPARATOR: string = ':'
export const TIME_RANGE_SEPARATOR: string = '—'

export const WEEKEND = [
  Weekday.SATURDAY,
  Weekday.SUNDAY,
]

export const WEEKDAYS_ORDER = [
  Weekday.MONDAY,
  Weekday.TUESDAY,
  Weekday.WEDNESDAY,
  Weekday.THURSDAY,
  Weekday.FRIDAY,
  Weekday.SATURDAY,
  Weekday.SUNDAY,
]
