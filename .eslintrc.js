module.exports = {
  root: true,
  env: { node: true },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: 'off',
    'class-methods-use-this': 'off',
    'object-curly-newline': ['error', {
      ObjectExpression: {
        multiline: true,
        minProperties: 2,
      },
      ObjectPattern: { multiline: true },
      ImportDeclaration: 'never',
      ExportDeclaration: {
        multiline: true,
        minProperties: 3,
      },
    }],
    'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
  },
  parserOptions: { parser: '@typescript-eslint/parser' },
}
