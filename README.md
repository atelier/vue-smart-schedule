# vue-smart-schedule
Attempt to implement the [solution of working hours schedule configurator](https://bureau.ru/bb/soviet/20180612/) problem by Ilya Birman.

Tech:
- TypeScript
- Vue

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
